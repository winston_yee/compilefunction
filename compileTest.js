'use strict';

const test = require('tape');
const tapSpec = require('tap-spec');
const fs = require('fs');
const compile = require('./compile-module');
const COMPILE_FILE = './compile.ts';

// Pretty output in console
test.createStream()
  .pipe(tapSpec())
  .pipe(process.stdout);

test('should extract interface', assert => {
  let content = fs.readFileSync(COMPILE_FILE, {encoding: 'utf8'});
  let code = compile.extractInterface(content);
  let output = 'export interface CancelRequest { RID:                number; ConfirmationNumber: number; }';
  assert.equal(code.join(''), output.join(' '));

  assert.end();
});

// test('should extract code', assert => {
//   let content = fs.readFileSync(COMPILE_FILE, {encoding: 'utf8'});
//   let code = compile.extractCode(content);
//   let output = 'function index(err, req, res) { let cancelReq: CancelRequest = req.body; let x = {}; let y = x* rid; res.status(200).json({saltedRid: 1}); }';
//   assert.equal(code.join(''), output.join(' '));
//
//   assert.end();
// });

/*
 Can write a compile function to check cancelReq is of correct type and
 to throw an error to prevent and
 object being multiplied by a number?
 */